<?php

/*
Plugin Name: Multisite User Sync
Plugin URI: https://shamimbiplob.wordpress.com/contact-us/
Description: Multisite User Sync will automatically synchronise users to all sites in multisite. Roles of users will be same on everysite. If Role change in one site it will also synchronise to all site. If new user/site created it will also add to all site/users.
Version: 2.3
Author: Shamim
Author URI: https://shamimbiplob.wordpress.com/contact-us/
License: GPLv2 or later
*/
